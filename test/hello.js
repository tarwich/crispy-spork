'use strict';
// jshint mocha: true

// Libraries
var _      = require('lodash');
var assert = require('assert');
var http   = require('http');
var net    = require('net');
// Local libraries
var config = require('crispy-spork/lib/config');

describe('server', function() {
  // Bootstrap the test to ensure the server is running
  before(function(done) {
    new Promise((resolve, reject) => {
      // Attempt to connect to the server
      var client = net.connect(config.web, resolve);
      // If the client connection ends, we'll consider that a success
      client.on('end', resolve);
      // On error, reject the promise
      client.on('error', reject);
    })
    .catch(error => {
      // Load the server
      require('crispy-spork/lib/app');
      // Need to re-resolve the promise to get it out of error state
      return Promise.resolve();
    })
    // If there are any more errors, then it's a big deal
    .catch(error => console.error(error))
    .then(done);
  });

  it('should be running and respond correctly', (next) => {
    return request(config.web)
    .then((response) => assert.equal(response.body, 'world'))
    .then(next, error => next(new Error(error)));
  });
});

/**
 * Call an HTTP endpoint
 */
function request(headers, options) {
  return new Promise(function(resolve, reject) {
    headers = _.defaultsDeep({}, headers, {
      method:  'GET',
      headers: {'Content-Type': 'application/json'}
    });

    var request = http.request(headers, function(response) {
      var data = '';

      // Pipe the response into our buffer
      response.on('data', chunk => data += chunk.toString());
      response.on('end', function() {
        // Get the data
        try { data = JSON.parse(data); }
        catch (e) { data = data; }

        // Status code 2XX
        if (response.statusCode >= 200 && response.statusCode < 300) {
          resolve({response: response, body: data});
        } else {
          reject(data);
        }
      });
    });

    // I don't know if this is necessary. It's possible that the errors will
    // already be thrown
    request.on('error', error => reject(new Error(error)));
    // Send a body if applicable
    if (headers.method != 'GET')
      request.write(JSON.stringify(options.body || {}));
    request.end();
  });
}
