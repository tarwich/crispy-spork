'use strict';
// jshint mocha: true

// Libraries
var assert = require('assert');
// Local libraries
var config; // Deffered loading

describe('config', function() {
  it('should load from require', next => {
    config = require('crispy-spork/lib/config');
    next();
  });

  it('should have a value for web.port', next => {
    assert(config.web, 'no web option in config');
    assert(config.web.port, 'no port set in config.web');
    next();
  });

  it('should have a value for web.hostname', next => {
    assert(config.web.hostname);
    next();
  });
});
