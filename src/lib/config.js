'use strict';

// Libraries
var _ = require('lodash');
// Variables
var configuration = {};

(function() {
  // Make a function to load a file and not error if it fails
  var tryRequire = file => { try { return require(file); } catch (e) {} };
  // Load the configuration files in the proper order
  configuration = _.defaultsDeep(
    {},
    // Load the environment configuration
    tryRequire('crispy-spork/../../config.environment.json'),
    tryRequire('crispy-spork/../../config.env.json'),
    // Load the base configuration file
    tryRequire('crispy-spork/../../config.json')
  );
})();

module.exports = configuration;
