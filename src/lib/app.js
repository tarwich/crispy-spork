'use strict';

// Libraries
var express = require('express');
var config = require('crispy-spork/lib/config');
var http = require('http');
// Variables
var app = express();
var Server = http.Server(app);

module.exports = app;

// Simple route to respond to request with our canned response
app.get('/', (request, response) => response.send(config.web.response));

Server.listen(config.web.port, function() {
  var url, address = Server.address();

  // Build the server url
  url = 'http://' + address.address.replace(/^::$/, 'localhost');
  // Add the port to the address unless it's 80 (the default port)
  if (address.port != 80) url += ':' + address.port;
  // Tell the log we're up and running
  // log.info('API server running at: %s', url);
});
