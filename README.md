# Test Project

## Configuration

## tl;dr

```sh
# Run this any time you pull
npm install
# Run this to verify everything is setup correctly
npm test
```

### npm install

Run `npm install` in order to setup this project. Any time you pull, you should
run `npm install` again as it contains *postinstall* scripts that do more than
simply installing more *node_modules*.

> NOTE: Windows users will need to run npm install in an administrative shell

### config.environment.json

Familiarize yourself with the `config.json` file in the root of the project. You
can create a `config.environment.json` file *(or `config.env.json` if you
prefer)* to override any setting in the config.json file. These settings are
merged recursively, so you don't need to repeat anything but keys that you
intend to change.

In order for the last test to pass, `config.environment.json` file requires
web.response to be "world". This can be set by creating a
`config.environment.json` file and populating it with this data:

```json
{
  "web": {
    "response": "world"
  }
}
```

### npm test

Unit tests have been provided to test the integrity of the project. To run them,
simply run `npm test`. If you have configured everything correctly, then all the
tests should pass.
