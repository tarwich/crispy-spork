'use strict';

var fs = require('fs');

var
  source      = '../src',
  destination = 'node_modules/crispy-spork';

fs.exists(destination, exists =>
  (exists) || fs.symlink(source, destination, 'dir', () => 0)
);
